package com.car360.sample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.car360.sample.R;
import com.egosventures.stable360.core.model.entities.CarIdentification;
import com.egosventures.stable360.display.model.LoadingEvent;
import com.egosventures.stable360.display.model.OnlineViewerConfiguration;
import com.egosventures.stable360.display.model.ReadyEvent;
import com.egosventures.stable360.display.views.OnlineSpinView;

public class CustomOnlineViewerActivity extends AppCompatActivity implements OnlineSpinView.OnlineSpinViewListener {

    public final static String EXTRA_CAR_IDENTIFICATION = "EXTRA_CAR_IDENTIFICATION";

    private OnlineSpinView mOnlineSpinView;
    private FrameLayout mLoadingContainer;
    private ScrollView mButtonsContainer;
    private Button mToggleInteriorButton;
    private Button mToggleTagsButton;
    private Button mSwitchSpinButton;
    private Button mZoomInButton;
    private Button mZoomOutButton;
    private Button mZoomResetButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_online_viewer);

        // Get the widgets:
        mOnlineSpinView = findViewById(R.id.onlineSpinView);
        mLoadingContainer = findViewById(R.id.loadingContainer);
        mButtonsContainer = findViewById(R.id.buttonsContainer);
        mZoomInButton = findViewById(R.id.zoomInButton);
        mZoomOutButton = findViewById(R.id.zoomOutButton);
        mZoomResetButton = findViewById(R.id.zoomResetButton);
        mSwitchSpinButton = findViewById(R.id.switchSpinButton);
        mToggleTagsButton = findViewById(R.id.toggleTagsButton);
        mToggleInteriorButton = findViewById(R.id.toggleInteriorButton);

        // Get the intent extra:
        CarIdentification carIdentification = getIntent().getParcelableExtra(EXTRA_CAR_IDENTIFICATION);

        // Create the viewer configuration:
        OnlineViewerConfiguration configuration = new OnlineViewerConfiguration();
        configuration.displayMode = OnlineViewerConfiguration.DisplayMode.NO_VDP_16_BY_9;
        configuration.overlayType = OnlineViewerConfiguration.OverlayType.NONE;

        mOnlineSpinView.setListener(this);
        mOnlineSpinView.loadSpin(carIdentification, configuration);

        // Listen clicks:
        mZoomInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnlineSpinView.zoomIn();
            }
        });

        mZoomOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnlineSpinView.zoomOut();
            }
        });

        mZoomResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnlineSpinView.zoomReset();
            }
        });

        mSwitchSpinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnlineSpinView.switchSpin();
            }
        });

        mToggleTagsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnlineSpinView.toggleTags();
            }
        });

        mToggleInteriorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnlineSpinView.toggleInterior();
            }
        });
    }

    @Override
    public void onReadyEvent(final ReadyEvent readyEvent) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                switch (readyEvent.getType()) {

                    case SPIN: {
                        mLoadingContainer.setVisibility(View.GONE);
                        mButtonsContainer.setVisibility(View.VISIBLE);
                        break;
                    }

                    case ZOOM: {
                        mZoomInButton.setEnabled(true);
                        mZoomOutButton.setEnabled(true);
                        mZoomResetButton.setEnabled(true);

                        break;
                    }

                    case SWITCH_SPIN: {
                        mSwitchSpinButton.setEnabled(true);
                        break;
                    }

                    case TAGS: {
                        mToggleTagsButton.setEnabled(true);
                        break;
                    }

                    case INTERIOR_EXISTS: {
                        mToggleInteriorButton.setVisibility(View.VISIBLE);
                        break;
                    }

                    case INTERIOR: {
                        mToggleInteriorButton.setEnabled(true);
                        break;
                    }
                }
            }
        });
    }

    @Override
    public void onLoadingEvent(final LoadingEvent loadingEvent) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                switch (loadingEvent.getType()) {

                    case LOADING_ERROR: {

                        // Handle the error:
                        Toast.makeText(CustomOnlineViewerActivity.this, "An occurred while loading the spin: " + loadingEvent.getError(), Toast.LENGTH_LONG).show();
                        finish();

                        break;
                    }
                }
            }
        });
    }
}
