package com.car360.sample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.car360.sample.R;
import com.egosventures.stable360.core.dialogs.DeleteTagDialog;
import com.egosventures.stable360.core.dialogs.EditTagDialog;
import com.egosventures.stable360.core.fragments.InteriorImageViewerFragment;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.InteriorImage;
import com.egosventures.stable360.core.model.entities.InteriorImageTag;
import com.egosventures.stable360.core.model.entities.Tag;
import com.egosventures.stable360.core.views.TagView;
import com.melnykov.fab.FloatingActionButton;

public class CustomInteriorImageViewerActivity extends AppCompatActivity implements InteriorImageViewerFragment.InteriorImageViewerFragmentLoadingListener, InteriorImageViewerFragment.InteriorImageViewerFragmentListener, TagView.TagViewListener, EditTagDialog.EditTagDialogListener, DeleteTagDialog.DeleteTagDialogListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public final static String EXTRA_INTERIOR_IMAGE = "EXTRA_INTERIOR_IMAGE";

    public final static int RESULT_DID_RESHOOT_INTERIOR_IMAGE = 10;
    public final static int RESULT_DID_DELETE_INTERIOR_IMAGE = 11;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static String DIALOG_EDIT_TAG = "DIALOG_EDIT_TAG";
    private final static String DIALOG_DELETE_TAG = "DIALOG_DELETE_TAG";

    private final static int REQUEST_ACTIVITY_TAG_MEDIA = 10;

    private InteriorImageViewerFragment mViewer;

    private Button mEditTagsButton;
    private Button mAddTagButton;
    private Button mOkButton;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Life cycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_interior_image_viewer);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the widgets:
        mEditTagsButton = findViewById(R.id.editTagsButton);
        mAddTagButton = findViewById(R.id.addTagButton);
        mOkButton = findViewById(R.id.okButton);

        // Get the intent data:
        InteriorImage interiorImage = getIntent().getParcelableExtra(EXTRA_INTERIOR_IMAGE);

        // Add the viewer:
        if (savedInstanceState == null) {

            Bundle bundle = new Bundle();
            bundle.putParcelable(InteriorImageViewerFragment.EXTRA_INTERIOR_IMAGE, interiorImage);

            mViewer = new InteriorImageViewerFragment();
            mViewer.setArguments(bundle);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.insideViewerContainer, mViewer, "INSIDE_VIEWER").commit();
        }
        else {

            mViewer = (InteriorImageViewerFragment) getSupportFragmentManager().findFragmentByTag("INSIDE_VIEWER");
        }

        mViewer.setLoadingListener(this);
        mViewer.setListener(this);

        // Listen clicks:
        mEditTagsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                onEditTagsButtonClick();
            }
        });

        mAddTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onAddTagButtonClick();
            }
        });

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onOkButtonClick();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Call super:
        super.onActivityResult(requestCode, resultCode, data);

        // Handle the result:
        switch (requestCode) {

            case REQUEST_ACTIVITY_TAG_MEDIA: {

                // Reload the spin tags:
                mViewer.reloadTags();

                break;
            }
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { finish(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onEditTagsButtonClick() {

        mEditTagsButton.setVisibility(View.GONE);
        mAddTagButton.setVisibility(View.VISIBLE);
        mOkButton.setVisibility(View.VISIBLE);
        mViewer.setTagsEditionModeActive(true);
    }

    private void onAddTagButtonClick() {

        if(mViewer.isAddingTag()) {

            mAddTagButton.setText("Add tag");
            mViewer.cancelNewTagCreation();
        }
        else if(mViewer.isMovingTag()) {

            mAddTagButton.setText("Add tag");
            mViewer.cancelTagPositionChange();
        }
        else {

            mAddTagButton.setText("Cancel");
            mViewer.startAddingNewTag();
        }
    }

    private void onOkButtonClick() {

        if(mViewer.isAddingTag()) {

            mAddTagButton.setText("Add tag");
            mViewer.confirmNewTagCreation();
        }
        else if(mViewer.isMovingTag()) {

            mAddTagButton.setText("Add tag");
            mViewer.confirmTagPositionChange();
        }
        else {

            mEditTagsButton.setVisibility(View.VISIBLE);
            mAddTagButton.setVisibility(View.GONE);
            mOkButton.setVisibility(View.GONE);
            mViewer.setTagsEditionModeActive(false);
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region InteriorImageViewerFragment.InteriorImageViewerFragmentLoadingListener

    @Override
    public void onLoadingDone() {

        mViewer.getTagView().setListener(this);
    }

    @Override
    public void onLoadingError(InteriorImageViewerFragment.LoadingError loadingError) {

    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region InteriorImageViewerFragment.InteriorImageViewerFragmentListener

    @Override
    public void onBackgroundClick(float v, float v1) {

    }

    @Override
    public void onBackgroundClickConfirmed(float v, float v1) {

    }

    @Override
    public void onNewTagIndicatorClick() {

        mAddTagButton.setText("Add tag");
        mViewer.confirmNewTagCreation();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region TagView.TagViewListener

    @Override
    public void onContentViewClicked(Tag tag) {

        startTagMediaActivityForTag(tag);
    }

    @Override
    public void onEditTagButtonClicked(Tag tag) {

        EditTagDialog editTagDialog = EditTagDialog.editTagInstance(tag);
        editTagDialog.setListener(this);
        editTagDialog.show(getSupportFragmentManager(), DIALOG_EDIT_TAG);
    }

    @Override
    public void onDeleteTagButtonClicked(Tag tag) {

        DeleteTagDialog deleteTagDialog = DeleteTagDialog.newInstance(tag);
        deleteTagDialog.setListener(this);
        deleteTagDialog.show(getSupportFragmentManager(), DIALOG_DELETE_TAG);
    }

    @Override
    public void onHandleTagMediaButtonClicked(Tag tag) {

        startTagMediaActivityForTag(tag);
    }

    @Override
    public void onMoveTagButtonClicked(Tag tag) {

        mAddTagButton.setText("Cancel");

        mViewer.startChangingTagPosition(tag);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region DeleteTagDialog.DeleteTagDialogListener

    @Override
    public void onDeleteTag(DeleteTagDialog deleteTagDialog, Tag tag) {

        // Delete the tag:
        mViewer.deleteTag(tag);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region EditTagDialog.EditTagDialogListener

    @Override
    public void onEditTag(EditTagDialog editTagDialog, Tag tag) {

        // Update the tag:
        mViewer.updateTag(tag);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    private void startTagMediaActivityForTag(Tag tag) {

        Intent intent = new Intent(this, CustomTagMediaActivity.class);
        intent.putExtra(CustomTagMediaActivity.EXTRA_INTERIOR_IMAGE, mViewer.getInteriorImage());
        intent.putExtra(CustomTagMediaActivity.EXTRA_TAG, tag);
        startActivityForResult(intent, REQUEST_ACTIVITY_TAG_MEDIA);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
