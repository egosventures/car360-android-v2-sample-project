package com.car360.sample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.car360.sample.R;
import com.egosventures.stable360.capture.fragments.SpinCaptureFragment;
import com.egosventures.stable360.capture.model.CameraSettings;
import com.egosventures.stable360.capture.services.FinalizationService;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.model.entities.Spin;

import java.util.ArrayList;
import java.util.Set;

public class CustomSpinCaptureActivity extends AppCompatActivity implements SpinCaptureFragment.SpinCaptureFragmentListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public final static String EXTRA_CAPTURE_LOCAL_ID = "EXTRA_CAPTURE_LOCAL_ID";
    public final static String EXTRA_SPIN_STANDARD_METADATA_ID = "EXTRA_SPIN_STANDARD_METADATA_ID";

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static String SPIN_CAPTURE_FRAGMENT_TAG = "SPIN_CAPTURE_FRAGMENT_TAG";

    private SpinCaptureFragment mSpinCaptureFragment;
    private FrameLayout mMessageContainer;
    private ProgressBar mMessageProgressBar;
    private ImageView mMessageImageView;
    private TextView mMessageTextView;
    private Button mStartStopButton;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_spin_capture);

        // Get the widgets:
        mMessageContainer = findViewById(R.id.messageContainer);
        mMessageProgressBar = findViewById(R.id.messageProgressBar);
        mMessageImageView = findViewById(R.id.messageImageView);
        mMessageTextView = findViewById(R.id.messageTextView);
        mStartStopButton = findViewById(R.id.startStopButton);

        // Get the intent data:
        long captureLocalId = getIntent().getLongExtra(EXTRA_CAPTURE_LOCAL_ID, -1);
        String spinStandardMetadataId = getIntent().getStringExtra(EXTRA_SPIN_STANDARD_METADATA_ID);

        if(captureLocalId == -1) { throw new IllegalArgumentException("Capture local id must be specified with the key EXTRA_SPIN_STANDARD_METADATA_ID"); }
        if(spinStandardMetadataId == null) { throw new IllegalArgumentException("Spin standard metadata id must be specified with the key EXTRA_SPIN_STANDARD_METADATA_ID"); }

        // Get the spin capture fragment:
        mSpinCaptureFragment = (SpinCaptureFragment) getSupportFragmentManager().findFragmentByTag(SPIN_CAPTURE_FRAGMENT_TAG);

        // If the fragment does not exist, create and add it:
        if(mSpinCaptureFragment == null) {

            Bundle arguments = new Bundle(4);
            arguments.putLong(SpinCaptureFragment.EXTRA_CAPTURE_LOCAL_ID, captureLocalId);
            arguments.putString(SpinCaptureFragment.EXTRA_SPIN_STANDARD_METADATA_ID, spinStandardMetadataId);
            arguments.putSerializable(SpinCaptureFragment.EXTRA_SPIN_CAPTURE_TYPE, Spin.CaptureType.WALK_AROUND);
            arguments.putBoolean(SpinCaptureFragment.EXTRA_SHOW_BRACKETS, true);

            mSpinCaptureFragment = SpinCaptureFragment.newInstance(arguments);

            getSupportFragmentManager().beginTransaction()
                    .add(com.egosventures.stable360.capture.R.id.spinCaptureFragmentContainer, mSpinCaptureFragment, SPIN_CAPTURE_FRAGMENT_TAG)
                    .commit();
        }

        mSpinCaptureFragment.setListener(this);

        // Listen button clicks:
        mStartStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onStartStopButtonClick();
            }
        });
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onStartStopButtonClick() {

        if(mSpinCaptureFragment.isCapturing()) {

            mStartStopButton.setText("START");
            mSpinCaptureFragment.stopCapture();
        }
        else {

            mStartStopButton.setText("STOP");
            mSpinCaptureFragment.startCapture();
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region SpinCaptureFragment.SpinCaptureFragmentListener

    @Override
    public void onFrameworkInitFailed(Stable360Framework.Stable360FrameworkError stable360FrameworkError) {

        showMessage(R.drawable.ic_action_warning_dark, "Sorry, the 360° SDK init failed (error: " + stable360FrameworkError.name() + ")");
    }

    @Override
    public void onInitFailed(SpinCaptureFragment.InitError initError) {

        showMessage(R.drawable.ic_action_warning_dark, "Sorry, the camera init failed (error: " + initError.name() + ")");
    }

    @Override
    public void onInvalidDeviceOrientation(SpinCaptureFragment.NeededRotation neededRotation) {

        switch (neededRotation) {

            case ROTATE_90_DEGREES_LEFT: showMessage(R.drawable.ic_action_rotate_left_dark, "Please rotate your device to the left."); break;
            case ROTATE_90_DEGREES_RIGHT: showMessage(R.drawable.ic_action_rotate_right_dark, "Please rotate your device to the right."); break;
            case ROTATE_180_DEGREES_LANDSCAPE: showMessage(R.drawable.ic_action_rotate_horizontal_dark, "Please rotate your device in the opposite orientation."); break;
            case ROTATE_180_DEGREES_PORTRAIT: showMessage(R.drawable.ic_action_rotate_horizontal_dark, "Please rotate your device in the opposite orientation."); break;
        }
    }

    @Override
    public void onOtherSpinBeingFinalized() {

        showMessage(null, "Another spin is being finalized, please wait...", true);
    }

    @Override
    public void onOtherSpinBeingStabilized() {

        showMessage(null, "Another spin is being stabilized, please wait...", true);
    }

    @Override
    public void onCameraWillOpen(Set<SpinCaptureFragment.Warning> warnings) {

    }

    @Override
    public void onCameraOpened(CameraSettings cameraSettings) {

        hideMessage();
    }

    @Override
    public void onOrientationChanged(float yaw, float pitch, float roll) {}

    @Override
    public void onCaptureDone() {

        showMessage(null, "Your spin is being finalized, please wait...", true);
    }

    @Override
    public void onFinalizationDone() {

        showMessage(null, "Your spin is being stabilized, please wait...", true);
    }

    @Override
    public void onFinalizationFailed(FinalizationService.FinalizationError finalizationError) {

        switch (finalizationError) {

            case NOT_ENOUGH_FRAMES: {

                showMessage(null, "Sorry, not enough frames have been taken to create a spin. Please retry by walking slower.", false);
                break;
            }

            case FAILED_TO_CREATE_SPIN_FRAMES:
            case SPIN_NOT_FOUND: {

                showMessage(R.drawable.ic_action_warning_dark, "Sorry, stabilization of your spin failed (" + finalizationError.name() + ")", false);
                break;
            }
        }
    }

    @Override
    public void onStabilizationDone(long spinId, ArrayList<String> autoExtractedStillImagesStandardsMetadataIds) {

        setResult(RESULT_OK);
        finish();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Messages

    private void showMessage(Integer iconResourceId, String message) {

        showMessage(iconResourceId, message, false);
    }

    private void showMessage(Integer iconResourceId, String message, boolean showProgressBar) {

        mMessageContainer.setVisibility(View.VISIBLE);
        mMessageProgressBar.setVisibility(showProgressBar ? View.VISIBLE : View.GONE);
        mMessageTextView.setText(message);

        if(iconResourceId != null) {
            mMessageImageView.setImageResource(iconResourceId);
        }
        else {
            mMessageImageView.setVisibility(View.GONE);
        }
    }

    private void hideMessage() {

        mMessageContainer.setVisibility(View.GONE);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
