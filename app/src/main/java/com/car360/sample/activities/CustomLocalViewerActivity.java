package com.car360.sample.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.car360.sample.R;
import com.egosventures.stable360.capture.activities.Add3dTagActivity;
import com.egosventures.stable360.capture.activities.Add3dTagConfirmationActivity;
import com.egosventures.stable360.capture.views.LocalSpinView;
import com.egosventures.stable360.core.Constants;
import com.egosventures.stable360.core.activities.TagMediaActivity;
import com.egosventures.stable360.core.dialogs.BasicDialog;
import com.egosventures.stable360.core.dialogs.DeleteTagDialog;
import com.egosventures.stable360.core.dialogs.EditTagDialog;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.Spin;
import com.egosventures.stable360.core.model.entities.SpinTag;
import com.egosventures.stable360.core.model.entities.SpinTagFrame;
import com.egosventures.stable360.core.model.entities.Tag;
import com.egosventures.stable360.core.utils.Log;
import com.egosventures.stable360.core.views.SpinView;
import com.egosventures.stable360.core.views.TagView;

public class CustomLocalViewerActivity extends AppCompatActivity implements TagView.TagViewListener, DeleteTagDialog.DeleteTagDialogListener, EditTagDialog.EditTagDialogListener, SpinView.SpinViewListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public final static String EXTRA_SPIN_LOCAL_ID = "EXTRA_SPIN_LOCAL_ID";

    public final static int REQUEST_ACTIVITY_ADD_3D_TAG = 10;
    public final static int REQUEST_ACTIVITY_TAG_MEDIA = 11;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static String DIALOG_EDIT_TAG = "DIALOG_EDIT_TAG";
    private final static String DIALOG_DELETE_TAG = "DIALOG_DELETE_TAG";

    private LocalSpinView mSpinView;
    private Button mAddTagButton;
    private Button mEditTagsButton;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_local_viewer);

        // Enable up navigation :
        if(getSupportActionBar() != null) { getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

        // Get the widgets:
        mSpinView = findViewById(R.id.spinView);
        mAddTagButton = findViewById(R.id.addTagButton);
        mEditTagsButton = findViewById(R.id.editTagsButton);

        // Get the intent data:
        long spinLocalId = getIntent().getLongExtra(EXTRA_SPIN_LOCAL_ID, -1);

        if(spinLocalId != -1) {

            // Get the spin:
            Spin spin = Stable360Db.getInstance(this).getWithId(Spin.class, spinLocalId);

            mSpinView.loadSpin(spin);
            mSpinView.setListener(this);
        }
        else {

            finish();
        }

        // Set the tag view listener:
        // This is needed only if you need to display your own dialogs for editing the tag title &
        // asking confirmation before deleting a tag. If you only need to replace the default
        // TagMediaActivity by your own, you can simply use the TagViewListener's
        // onTagMediaActivityWillBeStarted method.
        mSpinView.getTagView().setListener(this);

        // Listen clicks:
        mAddTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onAddTagButtonClick();
            }
        });

        mEditTagsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onEditTagsButtonClick();
            }
        });


        Constants.LOG_ENABLED = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Call super:
        super.onActivityResult(requestCode, resultCode, data);

        // Handle the result:
        switch (requestCode) {

            case REQUEST_ACTIVITY_ADD_3D_TAG: {

                switch (resultCode) {

                    case Add3dTagActivity.RESULT_SPIN_NOT_FOUND: Log.e(this, "RESULT_SPIN_NOT_FOUND"); break;
                    case Add3dTagActivity.RESULT_UNKNOWN_SPIN_DEGREES: Log.e(this, "RESULT_UNKNOWN_SPIN_DEGREES"); break;
                    case Add3dTagActivity.RESULT_BAD_GYRO_DATA: Log.e(this, "RESULT_BAD_GYRO_DATA"); break;
                    case Add3dTagActivity.RESULT_SPIN_NOT_COMPUTED_IN_3D: Log.e(this, "RESULT_SPIN_NOT_COMPUTED_IN_3D"); break;

                    case Activity.RESULT_OK: {

                        // The 3D tag creation succeeded, get the tag:
                        final SpinTag spinTag = data.getParcelableExtra(Add3dTagConfirmationActivity.EXTRA_TAG);

                        mSpinView.setHasMadeChangesInTagsEditionMode(true);
                        mSpinView.setImage(spinTag.getCenterFrameNumber());

                        mSpinView.reloadSpinTags(new Runnable() {
                            @Override
                            public void run() {

                                mSpinView.showTagViewForTag(spinTag, true, null);
                            }
                        });

                        break;
                    }
                }

                break;
            }

            case REQUEST_ACTIVITY_TAG_MEDIA: {

                // Reload the spin tags:
                mSpinView.reloadSpinTags(new Runnable() {
                    @Override
                    public void run() {

                        mSpinView.getTagView().reload();
                    }
                });

                break;
            }
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { onBackPressed(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onAddTagButtonClick() {

        Intent intent = new Intent(this, CustomAdd3dTagActivity.class);
        intent.putExtra(CustomAdd3dTagActivity.EXTRA_SPIN_LOCAL_ID, mSpinView.getSpin().getLocalId());
        startActivityForResult(intent, REQUEST_ACTIVITY_ADD_3D_TAG);
    }

    private void onEditTagsButtonClick() {

        if(mSpinView.isTagEditionModeActive()) {

            mSpinView.setTagEditionModeActive(false);
            mAddTagButton.setVisibility(View.GONE);
            mEditTagsButton.setText("Edit tags");
        }
        else {

            mSpinView.setTagEditionModeActive(true);
            mAddTagButton.setVisibility(View.VISIBLE);
            mEditTagsButton.setText("OK");
        }

        mSpinView.reloadSpinTags();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region SpinView listener

    @Override
    public void onSpinLoadingDone(SpinView spinView) {}

    @Override
    public void onSpinLoadingFailed(SpinView spinView, SpinView.LoadingError loadingError) {}

    @Override
    public void onBackgroundClick(SpinView spinView, float v, float v1) {}

    @Override
    public void onBackgroundClickConfirmed(SpinView spinView, float v, float v1) {}

    @Override
    public void onBackgroundDrag(SpinView spinView, float v, float v1) {}

    @Override
    public void onBackgroundDragEnd(SpinView spinView, float v, float v1) {}

    @Override
    public void onImageChanged(SpinView spinView, int i) {}

    @Override
    public void onTagDeleted(SpinView spinView) {}

    @Override
    public void onTagEditionModeStopped(SpinView spinView) {}

    @Override
    public void onTagViewShown(SpinView spinView) {}

    @Override
    public void onTagViewHidden(SpinView spinView) {}

    @Override
    public boolean onTagMediaActivityWillBeStarted(Tag tag) {

        startTagMediaActivityForTag(tag);
        return true;
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region TagView.TagViewListener

    @Override
    public void onContentViewClicked(Tag tag) {

        startTagMediaActivityForTag(tag);
    }

    @Override
    public void onEditTagButtonClicked(Tag tag) {

        EditTagDialog editTagDialog = EditTagDialog.editTagInstance(tag);
        editTagDialog.setListener(this);
        editTagDialog.show(getSupportFragmentManager(), DIALOG_EDIT_TAG);
    }

    @Override
    public void onDeleteTagButtonClicked(Tag tag) {

        DeleteTagDialog deleteTagDialog = DeleteTagDialog.newInstance(tag);
        deleteTagDialog.setListener(this);
        deleteTagDialog.show(getSupportFragmentManager(), DIALOG_DELETE_TAG);
    }

    @Override
    public void onHandleTagMediaButtonClicked(Tag tag) {

        startTagMediaActivityForTag(tag);
    }

    @Override
    public void onMoveTagButtonClicked(Tag tag) {

    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region DeleteTagDialog.DeleteTagDialogListener

    @Override
    public void onDeleteTag(DeleteTagDialog deleteTagDialog, Tag tag) {

        // Delete the tag:
        Stable360Db.getInstance(this).deleteWithId(SpinTag.class, tag.getLocalId());

        // Reload the tags:
        mSpinView.reloadSpinTags();

        // Hide the tag view:
        mSpinView.hideTagView();

        mSpinView.setHasMadeChangesInTagsEditionMode(true);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region EditTagDialog.EditTagDialogListener

    @Override
    public void onEditTag(EditTagDialog editTagDialog, Tag tag) {

        // Update the tag:
        Stable360Db.getInstance(this).update(tag, tag.getLocalId());

        // Reload the tags:
        mSpinView.reloadSpinTags();

        // Reload the tag view:
        if(mSpinView.getTagView().isVisible()) {
            mSpinView.showTagViewForTag((SpinTag)tag, false, null);
        }

        mSpinView.setHasMadeChangesInTagsEditionMode(true);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    private void startTagMediaActivityForTag(Tag tag) {

        Intent intent = new Intent(this, CustomTagMediaActivity.class);
        intent.putExtra(CustomTagMediaActivity.EXTRA_SPIN, mSpinView.getSpin());
        intent.putExtra(CustomTagMediaActivity.EXTRA_TAG, tag);
        startActivityForResult(intent, REQUEST_ACTIVITY_TAG_MEDIA);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
