package com.car360.sample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.car360.sample.R;
import com.egosventures.stable360.core.model.entities.CarIdentification;
import com.egosventures.stable360.display.activities.OnlineViewerActivity;

public class CaptureIdentificationActivity extends AppCompatActivity {

    private Spinner mCaptureIdentificationTypeSpinner;
    private EditText mCaptureIdentificationEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_capture_identification);

        // Get the widgets:
        mCaptureIdentificationTypeSpinner = findViewById(R.id.captureIdentificationTypeSpinner);
        mCaptureIdentificationEditText = findViewById(R.id.captureIdentificationValueEditText);

        // Populate the spinner:
        final String[] choices = new String[CarIdentification.CarIdentificationType.values().length];
        for(int i = 0; i < choices.length; i++) {
            choices[i] = CarIdentification.CarIdentificationType.values()[i].name();
        }

        ArrayAdapter<String> adapter =new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, choices);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCaptureIdentificationTypeSpinner.setAdapter(adapter);

        // Listen clicks:
        findViewById(R.id.defaultActivityButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDefaultButtonClick();
            }
        });

        findViewById(R.id.customActivityButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCustomButtonClick();
            }
        });
    }

    private void onDefaultButtonClick() {

        Intent intent = new Intent(this, OnlineViewerActivity.class);
        intent.putExtra(OnlineViewerActivity.EXTRA_CAR_IDENTIFICATION, getCarIdentification());
        startActivity(intent);
    }

    private void onCustomButtonClick() {

        Intent intent = new Intent(this, CustomOnlineViewerActivity.class);
        intent.putExtra(CustomOnlineViewerActivity.EXTRA_CAR_IDENTIFICATION, getCarIdentification());
        startActivity(intent);
    }

    private CarIdentification getCarIdentification() {

        String value = mCaptureIdentificationEditText.getText().toString();
        CarIdentification.CarIdentificationType type = CarIdentification.CarIdentificationType.values()[mCaptureIdentificationTypeSpinner.getSelectedItemPosition()];
        return new CarIdentification(value, type);
    }
}
