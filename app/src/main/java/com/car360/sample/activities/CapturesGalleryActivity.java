package com.car360.sample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.car360.sample.R;
import com.car360.sample.adapters.CapturesGalleryAdapter;
import com.egosventures.stable360.capture.services.UploadService;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.Capture;
import com.egosventures.stable360.core.model.entities.Spin;

public class CapturesGalleryActivity extends AppCompatActivity implements CapturesGalleryAdapter.CapturesGalleryAdapterListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static int REQUEST_CAPTURE_DETAILS = 0;

    private CapturesGalleryAdapter mAdapter;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_captures_gallery);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the widgets:
        RecyclerView capturesRecyclerView = findViewById(R.id.capturesRecyclerView);

        mAdapter = new CapturesGalleryAdapter(this, capturesRecyclerView, this);
        capturesRecyclerView.setAdapter(mAdapter);
        capturesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { finish(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region CapturesGalleryAdapterListener

    @Override
    public void onCaptureClick(long captureLocalId) {

        Intent intent = new Intent(this, CaptureDetailsActivity.class);
        intent.putExtra(CaptureDetailsActivity.EXTRA_CAPTURE_LOCAL_ID, captureLocalId);
        startActivityForResult(intent, REQUEST_CAPTURE_DETAILS);
    }

    @Override
    public void onDeleteCaptureButtonClick(long captureLocalId) {

        Capture.delete(this, captureLocalId);
        mAdapter.reloadData();
    }

    @Override
    public void onUploadCaptureButtonClick(long captureLocalId) {

        Capture capture = Stable360Db.getInstance(this).getWithId(Capture.class, captureLocalId);
        UploadService.upload(this, getSupportFragmentManager(), capture);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
