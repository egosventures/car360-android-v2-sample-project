package com.car360.sample.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.car360.sample.R;
import com.egosventures.stable360.capture.activities.LocalViewerActivity;
import com.egosventures.stable360.capture.activities.SpinCaptureActivity;
import com.egosventures.stable360.capture.activities.StillImageCaptureActivity;
import com.egosventures.stable360.capture.activities.StillImageViewerActivity;
import com.egosventures.stable360.capture.fragments.SpinCaptureFragment;
import com.egosventures.stable360.capture.services.FinalizationService;
import com.egosventures.stable360.core.Constants;
import com.egosventures.stable360.core.activities.InteriorImageSourceChooserActivity;
import com.egosventures.stable360.core.activities.InteriorImageViewerActivity;
import com.egosventures.stable360.core.dialogs.BasicDialog;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.Capture;
import com.egosventures.stable360.core.model.entities.InteriorImage;
import com.egosventures.stable360.core.model.entities.InteriorImageStandard;
import com.egosventures.stable360.core.model.entities.Spin;
import com.egosventures.stable360.core.model.entities.SpinStandard;
import com.egosventures.stable360.core.model.entities.StillImage;
import com.egosventures.stable360.core.model.entities.StillImageStandard;
import com.egosventures.stable360.core.model.entities.Tag;
import com.egosventures.stable360.core.utils.Callback;
import com.egosventures.stable360.core.utils.Utils;
import com.egosventures.stable360.core.utils.images_loader.BitmapDecoder;
import com.egosventures.stable360.core.utils.math.Size;
import com.egosventures.stable360.core.views.SpinView;

import java.util.ArrayList;

public class CaptureDetailsActivity extends AppCompatActivity implements SpinView.SpinViewListener, BasicDialog.BasicDialogListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public static final String EXTRA_CAPTURE_LOCAL_ID = "EXTRA_CAPTURE_LOCAL_ID";

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private static final String DIALOG_SPIN_VIEWER_CHOICE = "DIALOG_SPIN_VIEWER_CHOICE";
    private static final String DIALOG_SPIN_VIEWER_CHOICE_SPIN_LOCAL_ID = "DIALOG_SPIN_VIEWER_CHOICE_SPIN_LOCAL_ID";

    private static final String DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE = "DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE";
    private static final String DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE_INTERIOR_IMAGE = "DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE_INTERIOR_IMAGEN_LOCAL_ID";

    private static final String DIALOG_SPIN_CAPTURE_ACTIVITY_CHOICE = "DIALOG_SPIN_CAPTURE_ACTIVITY_CHOICE";
    private static final String DIALOG_INTERIOR_IMAGE_CAPTURE_ACTIVITY_CHOICE = "DIALOG_INTERIOR_IMAGE_CAPTURE_ACTIVITY_CHOICE";

    private static final int REQUEST_DEFAULT_SPIN_CAPTURE_ACTIVITY = 0;
    private static final int REQUEST_CUSTOM_SPIN_CAPTURE_ACTIVITY = 1;

    private static final int REQUEST_DEFAULT_INTERIOR_IMAGE_CAPTURE_ACTIVITY = 2;
    private static final int REQUEST_CUSTOM_INTERIOR_IMAGE_CAPTURE_ACTIVITY = 3;

    private static final int REQUEST_STILL_IMAGE_CAPTURE_ACTIVITY = 4;
    private static final int REQUEST_LOCAL_VIEWER_ACTIVITY = 5;

    private Capture mCapture;

    private LinearLayout mSpinStandardMainContainer;
    private FrameLayout mSpinStandardContainer;
    private TextView mSpinStandardTitleTextView;
    private SpinView mSpinView;
    private Button mDeleteSpinButton;

    private LinearLayout mInteriorImageStandardMainContainer;
    private FrameLayout mInteriorImageStandardContainer;
    private TextView mInteriorImageStandardTitleTextView;
    private ImageView mInteriorImageImageView;
    private Button mDeleteInteriorImageButton;

    private LinearLayout mStillImageStandardMainContainer;
    private FrameLayout mStillImageStandardContainer;
    private TextView mStillImageStandardTitleTextView;
    private ImageView mStillImageImageView;
    private Button mDeleteStillImageButton;

    private SpinStandard mSpinStandard;
    private InteriorImageStandard mInteriorImageStandard;
    private StillImageStandard mStillImageStandard;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate the layout:
        setContentView(R.layout.activity_capture_details);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the widgets:
        mSpinStandardMainContainer = findViewById(R.id.spinStandardMainContainer);
        mSpinStandardContainer = findViewById(R.id.spinStandardContainer);
        mSpinStandardTitleTextView = findViewById(R.id.spinStandardTitleTextView);
        mSpinView = findViewById(R.id.spinView);
        mDeleteSpinButton = findViewById(R.id.deleteSpinButton);

        mInteriorImageStandardMainContainer = findViewById(R.id.interiorImageStandardMainContainer);
        mInteriorImageStandardContainer = findViewById(R.id.interiorImageStandardContainer);
        mInteriorImageStandardTitleTextView = findViewById(R.id.interiorImageStandardTitleTextView);
        mInteriorImageImageView = findViewById(R.id.interiorImageImageView);
        mDeleteInteriorImageButton = findViewById(R.id.deleteInteriorImageButton);

        mStillImageStandardMainContainer = findViewById(R.id.stillImageStandardMainContainer);
        mStillImageStandardContainer = findViewById(R.id.stillImageStandardContainer);
        mStillImageStandardTitleTextView = findViewById(R.id.stillImageStandardTitleTextView);
        mStillImageImageView = findViewById(R.id.stillImageImageView);
        mDeleteStillImageButton = findViewById(R.id.deleteStillImageButton);

        mSpinView.setTagsClickable(false);

        // Get the intent data:
        long captureLocalId = getIntent().getLongExtra(EXTRA_CAPTURE_LOCAL_ID, -1);

        if(captureLocalId != -1) {

            // Get the data:
            Stable360Db db = Stable360Db.getInstance(this);

            mCapture = db.getWithId(Capture.class, captureLocalId);

            ArrayList<SpinStandard> spinStandards = db.getSpinStandardsWithWorkflowMetadataId(mCapture.getWorkflowMetadataId());
            ArrayList<InteriorImageStandard> interiorImageStandards = db.getInteriorImageStandardsWithWorkflowMetadataId(mCapture.getWorkflowMetadataId());
            ArrayList<StillImageStandard> stillImageStandards = db.getStillImageStandardsWithWorkflowMetadataId(mCapture.getWorkflowMetadataId());

            mSpinStandard = spinStandards.isEmpty() ? null : spinStandards.get(0);
            mInteriorImageStandard = interiorImageStandards.isEmpty() ? null : interiorImageStandards.get(0);
            mStillImageStandard = stillImageStandards.isEmpty() ? null : stillImageStandards.get(0);
        }
        else {

            Toast.makeText(this, "Sorry, this capture was not found", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        // Load the data:
        loadSpin();
        loadInteriorImage();
        loadStillImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Call super:
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REQUEST_DEFAULT_SPIN_CAPTURE_ACTIVITY: {

                switch (resultCode) {

                    case RESULT_OK: {

                        setResult(RESULT_OK, data);
                        finish();
                        break;
                    }

                    case SpinCaptureActivity.RESULT_FRAMEWORK_INIT_FAILED: { Toast.makeText(this, "Sorry, an error occurred when initialising the 360° features. Please restart the app. If the problem persists, please contact the support.", Toast.LENGTH_LONG).show(); break; }
                    case SpinCaptureActivity.RESULT_NULL_USER: { Toast.makeText(this, "Sorry, your session expired. Please restart the app. If the problem persists, please contact the support", Toast.LENGTH_LONG).show(); break; }

                    case SpinCaptureActivity.RESULT_INIT_FAILED: {

                        SpinCaptureFragment.InitError error = (SpinCaptureFragment.InitError) data.getSerializableExtra(SpinCaptureActivity.EXTRA_INIT_ERROR);

                        switch (error) {

                            case IO_ERROR:
                            case CAPTURE_NOT_FOUND:
                            case SPIN_STANDARD_NOT_FOUND: { Toast.makeText(this, "An error occurred while starting the capture, please retry later. If the problem persists, please contact the support", Toast.LENGTH_LONG).show(); break; }

                            case CAMERA_PERMISSION_DENIED: { Toast.makeText(this, "Sorry, camera permission is needed to take a spin", Toast.LENGTH_LONG).show(); break; }
                            case UNSUPPORTED_PARTIAL_CAPTURE_TYPE: { Toast.makeText(this, "Sorry, your device does not support taking a partial spin, because it has no gyroscope", Toast.LENGTH_LONG).show(); break; }
                            case INSUFFICIENT_DISK_SPACE: {

                                String text = "Sorry, you need at least " + Formatter.formatFileSize(this, Constants.MIN_FREE_DISK_SPACE_TO_TAKE_SPIN) + " of available disk space to start a new capture";
                                Toast.makeText(this, text, Toast.LENGTH_LONG).show();
                                break;
                            }
                        }

                        break;
                    }

                    case SpinCaptureActivity.RESULT_FINALIZATION_FAILED: {

                        FinalizationService.FinalizationError error = (FinalizationService.FinalizationError) data.getSerializableExtra(SpinCaptureActivity.EXTRA_FINALIZATION_ERROR);

                        switch (error) {

                            case SPIN_NOT_FOUND: { Toast.makeText(this, "Sorry, an error occurred while generating the spin data", Toast.LENGTH_LONG).show(); break; }
                            case FAILED_TO_CREATE_SPIN_FRAMES: { Toast.makeText(this, "Sorry, an error occurred while generating the spin data", Toast.LENGTH_LONG).show(); break; }
                            case NOT_ENOUGH_FRAMES: { Toast.makeText(this, "Not enough frames have been captured to create a spin", Toast.LENGTH_LONG).show(); break; }
                        }

                        break;
                    }
                }

                break;
            }

            case REQUEST_CUSTOM_INTERIOR_IMAGE_CAPTURE_ACTIVITY: {

                switch (resultCode) {

                    case CustomInteriorImageCaptureActivity.RESULT_PICTURE_SAVED: {

                        loadInteriorImage();
                    }
                }
            }
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { finish(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    private void loadSpin() {

        if(mSpinStandard == null) {
            mSpinStandardMainContainer.setVisibility(View.GONE);
        }
        else {

            mSpinStandardTitleTextView.setText(mSpinStandard.getName());

            mSpinStandardContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSpinStandardClick();
                }
            });

            final Spin spin = Stable360Db.getInstance(this).getSpinInCaptureWithSpinStandardMetadataId(mCapture.getLocalId(), mSpinStandard.getMetadata().getId());

            if(spin != null) {

                mSpinView.setVisibility(View.VISIBLE);
                mSpinView.loadSpin(spin);
                mSpinView.setListener(this);

                mDeleteSpinButton.setVisibility(View.VISIBLE);
                mDeleteSpinButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Spin.delete(CaptureDetailsActivity.this, spin.getLocalId());
                        loadSpin();
                    }
                });
            }
            else {

                mDeleteSpinButton.setVisibility(View.GONE);
                mSpinView.setVisibility(View.GONE);
            }
        }
    }

    private void loadInteriorImage() {

        if(mInteriorImageStandard == null) {
            mInteriorImageStandardMainContainer.setVisibility(View.GONE);
        }
        else {

            mInteriorImageImageView.setVisibility(View.VISIBLE);

            final InteriorImage interiorImage = Stable360Db.getInstance(this).getInteriorImageOfCaptureWithInteriorImageStandardMetadataId(mCapture.getLocalId(), mInteriorImageStandard.getMetadata().getId());

            if(interiorImage != null) {

                final String path = interiorImage.getFile(this).getAbsolutePath();

                Utils.getViewSize(mInteriorImageImageView, new Callback<Size>() {
                    @Override
                    public void onResult(Size size) {

                        Bitmap bitmap = BitmapDecoder.decodeSampledBitmapFromFile(path, size, true);
                        mInteriorImageImageView.setImageBitmap(bitmap);
                    }
                });

                mDeleteInteriorImageButton.setVisibility(View.VISIBLE);
                mDeleteInteriorImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        InteriorImage.delete(CaptureDetailsActivity.this, interiorImage.getLocalId());
                        loadInteriorImage();
                    }
                });
            }
            else {

                mDeleteInteriorImageButton.setVisibility(View.GONE);
                mInteriorImageImageView.setVisibility(View.GONE);
            }

            mInteriorImageStandardTitleTextView.setText(mInteriorImageStandard.getName());

            mInteriorImageStandardContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(interiorImage != null) {
                        onInteriorImageClick(interiorImage);
                    }
                    else {
                        onInteriorImageStandardClick();
                    }
                }
            });
        }
    }

    private void loadStillImage() {

        if(mStillImageStandard == null) {

            mStillImageStandardMainContainer.setVisibility(View.GONE);
        }
        else {

            mStillImageImageView.setVisibility(View.VISIBLE);

            final StillImage stillImage = Stable360Db.getInstance(this).getStillImageOfCaptureWithStillImageStandardMetadataId(mCapture.getLocalId(), mStillImageStandard.getMetadata().getId());

            if(stillImage != null) {

                final String path = stillImage.getFile(this).getAbsolutePath();

                Utils.getViewSize(mStillImageImageView, new Callback<Size>() {
                    @Override
                    public void onResult(Size size) {

                        Bitmap bitmap = BitmapDecoder.decodeSampledBitmapFromFile(path, size, true);
                        mStillImageImageView.setImageBitmap(bitmap);
                    }
                });

                mDeleteStillImageButton.setVisibility(View.VISIBLE);
                mDeleteStillImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        StillImage.delete(CaptureDetailsActivity.this, stillImage.getLocalId());
                        loadStillImage();
                    }
                });
            }
            else {

                mDeleteStillImageButton.setVisibility(View.GONE);
                mStillImageImageView.setVisibility(View.GONE);
            }

            mStillImageStandardContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(stillImage != null) {
                        onStillImageClick(stillImage);
                    }
                    else {
                        onStillImageStandardClick();
                    }
                }
            });

            mStillImageStandardTitleTextView.setText(mStillImageStandard.getName());
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onSpinStandardClick() {

        BasicDialog.newInstance(null,
                "What do you want to do?",
                "Do you want to take the spin with the default activity or with the custom one?",
                "Default",
                "Custom")
                .show(this, DIALOG_SPIN_CAPTURE_ACTIVITY_CHOICE);
    }

    private void onSpinClick(Spin spin) {

        Bundle data = new Bundle();
        data.putLong(DIALOG_SPIN_VIEWER_CHOICE_SPIN_LOCAL_ID, spin.getLocalId());

        BasicDialog.newInstance(null,
                "What do you want to do?",
                "Do you want to open the spin in the default viewer or in a custom activity?",
                "Default",
                "Custom",
                data)
                .show(this, DIALOG_SPIN_VIEWER_CHOICE);
    }

    private void onInteriorImageStandardClick() {

        BasicDialog.newInstance(null,
                "What do you want to do?",
                "Do you want to take the interior image with the default activity or with the custom one?",
                "Default",
                "Custom")
                .show(this, DIALOG_INTERIOR_IMAGE_CAPTURE_ACTIVITY_CHOICE);
    }

    private void onInteriorImageClick(InteriorImage interiorImage) {

        Bundle data = new Bundle();
        data.putParcelable(DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE_INTERIOR_IMAGE, interiorImage);

        BasicDialog.newInstance(null,
                "What do you want to do?",
                "Do you want to open the interior image in the default viewer or in a custom activity?",
                "Default",
                "Custom",
                data)
                .show(this, DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE);
    }

    private void onStillImageStandardClick() {

        Intent intent = new Intent(this, StillImageCaptureActivity.class);
        intent.putExtra(StillImageCaptureActivity.EXTRA_CAPTURE, mCapture);
        intent.putExtra(StillImageCaptureActivity.EXTRA_STILL_IMAGE_STANDARD, mStillImageStandard);
        startActivityForResult(intent, REQUEST_STILL_IMAGE_CAPTURE_ACTIVITY);
    }

    private void onStillImageClick(StillImage stillImage) {

        Intent intent = new Intent(this, StillImageViewerActivity.class);
        intent.putExtra(StillImageViewerActivity.EXTRA_CAPTURE, mCapture);
        intent.putExtra(StillImageViewerActivity.EXTRA_STILL_IMAGE, stillImage);
        startActivity(intent);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region BasicDialog listener

    @Override
    public void onPositiveButtonClick(BasicDialog basicDialog) {

        if(basicDialog.getTag() != null) {

            switch (basicDialog.getTag()) {

                case DIALOG_SPIN_VIEWER_CHOICE: {

                    Bundle data = basicDialog.getData();

                    if(data != null) {

                        long spinLocalId = data.getLong(DIALOG_SPIN_VIEWER_CHOICE_SPIN_LOCAL_ID, -1);

                        if(spinLocalId != -1) {

                            Intent intent = new Intent(this, LocalViewerActivity.class);
                            intent.putExtra(LocalViewerActivity.EXTRA_SPIN_LOCAL_ID, spinLocalId);
                            startActivityForResult(intent, REQUEST_LOCAL_VIEWER_ACTIVITY);
                        }
                    }

                    break;
                }

                case DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE: {

                    Bundle data = basicDialog.getData();

                    if(data != null) {

                        InteriorImage interiorImage = data.getParcelable(DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE_INTERIOR_IMAGE);

                        if(interiorImage != null) {

                            Intent intent = new Intent(this, InteriorImageViewerActivity.class);
                            intent.putExtra(InteriorImageViewerActivity.EXTRA_INTERIOR_IMAGE, interiorImage);
                            intent.putExtra(InteriorImageViewerActivity.EXTRA_CAN_RECAPTURE, true);
                            intent.putExtra(InteriorImageViewerActivity.EXTRA_CAN_DELETE, true);
                            intent.putExtra(InteriorImageViewerActivity.EXTRA_CAN_START_TAGS_EDITION_MODE, true);
                            startActivity(intent);
                        }
                    }

                    break;
                }

                case DIALOG_SPIN_CAPTURE_ACTIVITY_CHOICE: {

                    Intent intent = new Intent(this, SpinCaptureActivity.class);
                    intent.putExtra(SpinCaptureActivity.EXTRA_CAPTURE_LOCAL_ID, mCapture.getLocalId());
                    intent.putExtra(SpinCaptureActivity.EXTRA_SPIN_STANDARD_METADATA_ID, mSpinStandard.getMetadata().getId());
                    intent.putExtra(SpinCaptureActivity.EXTRA_SPIN_CAPTURE_TYPE, Spin.CaptureType.WALK_AROUND);
                    intent.putExtra(SpinCaptureActivity.EXTRA_SHOW_CAMERA_SETTINGS, true);
                    startActivityForResult(intent, REQUEST_DEFAULT_SPIN_CAPTURE_ACTIVITY);

                    break;
                }

                case DIALOG_INTERIOR_IMAGE_CAPTURE_ACTIVITY_CHOICE: {

                    Intent intent = new Intent(this, InteriorImageSourceChooserActivity.class);
                    intent.putExtra(InteriorImageSourceChooserActivity.EXTRA_CAPTURE, mCapture);
                    intent.putExtra(InteriorImageSourceChooserActivity.EXTRA_INTERIOR_IMAGE_STANDARD, mInteriorImageStandard);
                    startActivityForResult(intent, REQUEST_DEFAULT_INTERIOR_IMAGE_CAPTURE_ACTIVITY);

                    break;
                }
            }
        }
    }

    @Override
    public void onNegativeButtonClick(BasicDialog basicDialog) {

        if(basicDialog.getTag() != null) {

            switch (basicDialog.getTag()) {

                case DIALOG_SPIN_VIEWER_CHOICE: {

                    Bundle data = basicDialog.getData();

                    if(data != null) {

                        long spinLocalId = data.getLong(DIALOG_SPIN_VIEWER_CHOICE_SPIN_LOCAL_ID, -1);

                        if(spinLocalId != -1) {

                            Intent intent = new Intent(this, CustomLocalViewerActivity.class);
                            intent.putExtra(CustomLocalViewerActivity.EXTRA_SPIN_LOCAL_ID, spinLocalId);
                            startActivityForResult(intent, REQUEST_LOCAL_VIEWER_ACTIVITY);
                        }
                    }

                    break;
                }

                case DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE: {

                    Bundle data = basicDialog.getData();

                    if(data != null) {

                        InteriorImage interiorImage = data.getParcelable(DIALOG_INTERIOR_IMAGE_VIEWER_CHOICE_INTERIOR_IMAGE);

                        if(interiorImage != null) {

                            Intent intent = new Intent(this, CustomInteriorImageViewerActivity.class);
                            intent.putExtra(CustomInteriorImageViewerActivity.EXTRA_INTERIOR_IMAGE, interiorImage);
                            startActivity(intent);
                        }
                    }

                    break;
                }

                case DIALOG_SPIN_CAPTURE_ACTIVITY_CHOICE: {

                    Intent intent = new Intent(this, CustomSpinCaptureActivity.class);
                    intent.putExtra(CustomSpinCaptureActivity.EXTRA_CAPTURE_LOCAL_ID, mCapture.getLocalId());
                    intent.putExtra(CustomSpinCaptureActivity.EXTRA_SPIN_STANDARD_METADATA_ID, mSpinStandard.getMetadata().getId());
                    startActivityForResult(intent, REQUEST_CUSTOM_SPIN_CAPTURE_ACTIVITY);

                    break;
                }

                case DIALOG_INTERIOR_IMAGE_CAPTURE_ACTIVITY_CHOICE: {

                    Intent intent = new Intent(this, CustomInteriorImageCaptureActivity.class);
                    intent.putExtra(CustomInteriorImageCaptureActivity.EXTRA_CAPTURE, mCapture);
                    intent.putExtra(CustomInteriorImageCaptureActivity.EXTRA_INTERIOR_IMAGE_STANDARD, mInteriorImageStandard);
                    startActivityForResult(intent, REQUEST_CUSTOM_INTERIOR_IMAGE_CAPTURE_ACTIVITY);

                    break;
                }
            }
        }
    }

    @Override
    public void onNeutralButtonClick(BasicDialog basicDialog) {}

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region SpinViewListener

    @Override
    public void onSpinLoadingDone(SpinView spinView) {}

    @Override
    public void onSpinLoadingFailed(SpinView spinView, SpinView.LoadingError loadingError) {}

    @Override
    public void onBackgroundClick(SpinView spinView, float v, float v1) {

        onSpinClick(spinView.getSpin());
    }

    @Override
    public void onBackgroundClickConfirmed(SpinView spinView, float v, float v1) {}

    @Override
    public void onBackgroundDrag(SpinView spinView, float v, float v1) {}

    @Override
    public void onBackgroundDragEnd(SpinView spinView, float v, float v1) {}

    @Override
    public void onImageChanged(SpinView spinView, int i) {}

    @Override
    public void onTagDeleted(SpinView spinView) {}

    @Override
    public void onTagEditionModeStopped(SpinView spinView) {}

    @Override
    public void onTagViewShown(SpinView spinView) {}

    @Override
    public void onTagViewHidden(SpinView spinView) {}

    @Override
    public boolean onTagMediaActivityWillBeStarted(Tag tag) { return false; }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
