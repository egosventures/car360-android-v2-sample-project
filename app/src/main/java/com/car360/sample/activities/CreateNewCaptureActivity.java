package com.car360.sample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.car360.sample.R;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.Stable360FrameworksHelper;
import com.egosventures.stable360.core.dialogs.BasicDialog;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.Capture;
import com.egosventures.stable360.core.model.entities.CarIdentification;
import com.egosventures.stable360.core.model.entities.Workflow;
import com.egosventures.stable360.core.services.DataSyncService;
import com.egosventures.stable360.core.utils.Callback;

import java.util.ArrayList;

public class CreateNewCaptureActivity extends AppCompatActivity implements BasicDialog.BasicDialogListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public final static int REQUEST_CAPTURE_DETAILS = 0;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static String TAG = "CreateNewCaptureAct";

    private final static String DIALOG_CAMERA_ACTIVITY_CHOICE = "DIALOG_CAMERA_ACTIVITY_CHOICE";
    private final static String DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_WORKFLOW = "DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_WORKFLOW";
    private final static String DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_CAR_IDENTIFICATION = "DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_CAR_IDENTIFICATION";

    private final static String STATE_SELECTED_WORKFLOW_INDEX = "STATE_SELECTED_WORKFLOW_INDEX";

    private ArrayList<Workflow> mWorkflows;
    private Spinner mWorkflowsSpinner;
    private EditText mCarIdentificationEditText;

    private BroadcastReceiver mDataSyncServiceReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.i(TAG, "Data sync done");

            reloadWorkflowsSpinner();
        }
    };

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate the layout:
        setContentView(R.layout.activity_create_new_capture);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the widgets:
        mWorkflowsSpinner = findViewById(R.id.workflowSpinner);
        mCarIdentificationEditText = findViewById(R.id.carIdentificationEditText);

        // Listen clicks:
        findViewById(R.id.nextButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onNextButtonClick();
            }
        });

        // If the data sync is done, load the workflows:
        DataSyncService.getGlobalSyncState(this, new Callback<DataSyncService.SyncState>() {

            @Override
            public void onResult(DataSyncService.SyncState syncState) {

                if(syncState == DataSyncService.SyncState.SUCCEEDED) {
                    reloadWorkflowsSpinner();

                    if(savedInstanceState != null) {
                        mWorkflowsSpinner.setSelection(savedInstanceState.getInt(STATE_SELECTED_WORKFLOW_INDEX));
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mDataSyncServiceReceiver, new IntentFilter(DataSyncService.BROADCAST_DATA_SYNC_DONE));
    }

    @Override
    protected void onPause() {

        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDataSyncServiceReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putInt(STATE_SELECTED_WORKFLOW_INDEX, mWorkflowsSpinner.getSelectedItemPosition());

        super.onSaveInstanceState(outState);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { finish(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onNextButtonClick() {

        Workflow workflow = getSelectedWorkflow();
        CarIdentification carIdentification = getCarIdentification();

        if(workflow != null && carIdentification != null) {

            Bundle data = new Bundle();
            data.putParcelable(DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_WORKFLOW, workflow);
            data.putParcelable(DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_CAR_IDENTIFICATION, carIdentification);

            BasicDialog.newInstance(null,
                    "What do you want to do?",
                    "Do you want to take the spin in the default camera activity or in a custom one?",
                    "Default",
                    "Custom",
                    data)
                    .show(this, DIALOG_CAMERA_ACTIVITY_CHOICE);
        }
        else {
            Toast.makeText(this, "Please choose a workflow and enter a valid car identification (alphanumeric characters only).", Toast.LENGTH_LONG).show();
        }
    }

    private void onStartDefaultCameraActivityButtonClick(Workflow workflow, CarIdentification carIdentification) {

        long captureLocalId = Capture.create(this, workflow, carIdentification);

        if(captureLocalId != -1) {

            Intent intent = new Intent(this, CaptureDetailsActivity.class);
            intent.putExtra(CaptureDetailsActivity.EXTRA_CAPTURE_LOCAL_ID, captureLocalId);
            startActivityForResult(intent, REQUEST_CAPTURE_DETAILS);
        }
        else {

            Toast.makeText(this, "Sorry, an error occurred while creating the capture.", Toast.LENGTH_LONG).show();
        }
    }

    private void onStartCustomCameraActivityButtonClick(Workflow workflow, CarIdentification carIdentification) {

        long captureLocalId = Capture.create(this, workflow, carIdentification);

        if(captureLocalId != -1) {

            Toast.makeText(this, "Start custom cam act with workflow " + workflow.getName() + " and car id " + carIdentification.getValue(), Toast.LENGTH_LONG).show();

            /*
            Intent intent = new Intent(this, CaptureDetailsActivity.class);
            intent.putExtra(CaptureDetailsActivity.EXTRA_CAPTURE_LOCAL_ID, captureLocalId);
            startActivityForResult(intent, REQUEST_CAPTURE_DETAILS);
            */
        }
        else {

            Toast.makeText(this, "Sorry, an error occurred while creating the capture.", Toast.LENGTH_LONG).show();
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region BasicDialogListener

    @Override
    public void onPositiveButtonClick(BasicDialog basicDialog) {

        if(basicDialog.getTag() != null) {

            switch (basicDialog.getTag()) {

                case DIALOG_CAMERA_ACTIVITY_CHOICE: {

                    Workflow workflow = basicDialog.getData().getParcelable(DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_WORKFLOW);
                    CarIdentification carIdentification = basicDialog.getData().getParcelable(DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_CAR_IDENTIFICATION);

                    onStartDefaultCameraActivityButtonClick(workflow, carIdentification);

                    break;
                }
            }
        }
    }

    @Override
    public void onNegativeButtonClick(BasicDialog basicDialog) {

        if(basicDialog.getTag() != null) {

            switch (basicDialog.getTag()) {

                case DIALOG_CAMERA_ACTIVITY_CHOICE: {

                    Workflow workflow = basicDialog.getData().getParcelable(DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_WORKFLOW);
                    CarIdentification carIdentification = basicDialog.getData().getParcelable(DIALOG_CAMERA_ACTIVITY_CHOICE_EXTRA_CAR_IDENTIFICATION);

                    onStartCustomCameraActivityButtonClick(workflow, carIdentification);

                    break;
                }
            }
        }
    }

    @Override
    public void onNeutralButtonClick(BasicDialog basicDialog) {}

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    private Workflow getSelectedWorkflow() {

        int workflowIndex = mWorkflowsSpinner.getSelectedItemPosition() -1;

        if(workflowIndex > 0 && workflowIndex < mWorkflows.size()) {
            return mWorkflows.get(workflowIndex);
        }

        return null;
    }

    private CarIdentification getCarIdentification() {

        String carIdentificationValue = mCarIdentificationEditText.getText().toString();

        if(carIdentificationValue.matches("^[a-zA-Z0-9]*$")) {
            return new CarIdentification(carIdentificationValue, CarIdentification.CarIdentificationType.EXTERNAL_ID, null);
        }

        return null;
    }

    private void reloadWorkflowsSpinner() {

        mWorkflows = Stable360Db.getInstance(this).getAll(Workflow.class);

        String[] names = new String[mWorkflows.size() + 1];
        names[0] = "";

        for (int i = 0; i < mWorkflows.size(); i ++) {
            Workflow workflow = mWorkflows.get(i);
            names[i + 1] = workflow.getName();
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
        mWorkflowsSpinner.setAdapter(adapter);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
