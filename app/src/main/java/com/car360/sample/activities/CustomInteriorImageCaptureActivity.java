package com.car360.sample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.car360.sample.R;
import com.egosventures.stable360.core.fragments.InteriorImageCaptureFragment;
import com.egosventures.stable360.core.model.entities.Capture;
import com.egosventures.stable360.core.model.entities.InteriorImage;
import com.egosventures.stable360.core.model.entities.InteriorImageStandard;
import com.egosventures.stable360.core.utils.osc.Cameras;
import com.egosventures.stable360.core.utils.osc.OscClient;
import com.egosventures.stable360.core.utils.osc.OscClientProvider;

public class CustomInteriorImageCaptureActivity extends AppCompatActivity implements InteriorImageCaptureFragment.InteriorImageCaptureFragmentListener  {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public final static String EXTRA_CAPTURE = "EXTRA_CAPTURE";
    public final static String EXTRA_INTERIOR_IMAGE_STANDARD = "EXTRA_INTERIOR_IMAGE_STANDARD";

    public final static int RESULT_PICTURE_SAVED = 10;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private InteriorImageCaptureFragment mInteriorImageCaptureFragment;
    private RelativeLayout mMessageContainer;
    private ProgressBar mMessageProgressBar;
    private TextView mMessageTextView;
    private TextView mWarningTextView;
    private Button mCaptureButton;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate the layout:
        setContentView(R.layout.activity_custom_interior_image_capture);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the widgets:
        mMessageContainer = findViewById(R.id.messageContainer);
        mMessageTextView = findViewById(R.id.messageTextView);
        mMessageProgressBar = findViewById(R.id.messageProgressBar);
        mWarningTextView = findViewById(R.id.warningTextView);
        mCaptureButton = findViewById(R.id.captureButton);

        // Get the params:
        Capture capture = getIntent().getParcelableExtra(EXTRA_CAPTURE);
        InteriorImageStandard interiorImageStandard = getIntent().getParcelableExtra(EXTRA_INTERIOR_IMAGE_STANDARD);

        // Add the fragment:
        if (savedInstanceState == null) {

            Bundle bundle = new Bundle();
            bundle.putParcelable(InteriorImageCaptureFragment.EXTRA_CAMERA, Cameras.ricohTheta());
            bundle.putSerializable(InteriorImageCaptureFragment.EXTRA_PREFERRED_API_LEVEL, OscClientProvider.OscApiLevel.API_LEVEL_2);
            bundle.putParcelable(InteriorImageCaptureFragment.EXTRA_CAPTURE, capture);
            bundle.putParcelable(InteriorImageCaptureFragment.EXTRA_INTERIOR_IMAGE_STANDARD, interiorImageStandard);

            mInteriorImageCaptureFragment = new InteriorImageCaptureFragment();
            mInteriorImageCaptureFragment.setArguments(bundle);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.oscCameraCaptureFragmentContainer, mInteriorImageCaptureFragment, "OSC_CAMERA_CAPTURE_FRAGMENT").commit();
        }
        else {

            mInteriorImageCaptureFragment = (InteriorImageCaptureFragment) getSupportFragmentManager().findFragmentByTag("OSC_CAMERA_CAPTURE_FRAGMENT");
        }

        mInteriorImageCaptureFragment.setListener(this);

        mCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCaptureButtonClick();
            }
        });
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { onBackPressed(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onCaptureButtonClick() {

        showMessage("Taking picture...", true);
        mInteriorImageCaptureFragment.takePicture();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    private void showMessage(String text, boolean showProgressBar) {

        mMessageContainer.setVisibility(View.VISIBLE);

        if(text != null) {

            mMessageTextView.setText(text);
            mMessageTextView.setVisibility(View.VISIBLE);
        }
        else {

            mMessageTextView.setVisibility(View.GONE);
        }

        mMessageProgressBar.setVisibility(showProgressBar ? View.VISIBLE : View.GONE);

        hideWarning();
    }

    private void hideMessage() {

        mMessageContainer.setVisibility(View.GONE);
    }

    private void showWarning(String text) {

        mWarningTextView.setVisibility(View.VISIBLE);
        mWarningTextView.setText(text);
        hideMessage();
    }

    private void hideWarning() {

        mWarningTextView.setVisibility(View.GONE);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region InteriorImageCaptureFragment.InteriorImageCaptureFragmentListener

    @Override
    public void onConnected() {

        // In case the camera was in another mode:
        mInteriorImageCaptureFragment.setExposureProgram(OscClient.ExposureProgram.AUTO);
    }

    @Override
    public void onConnectionFailed(Exception e) {

        showMessage("Sorry, the connection to the camera failed.", false);
    }

    @Override
    public void onLivePreviewConnected() {
        mCaptureButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLivePreviewUnsupported() {

        mCaptureButton.setVisibility(View.VISIBLE);
        showWarning("Live preview is not supported. You should still be able to take pictures.");
    }

    @Override
    public void onLivePreviewConnectionFailed(Exception e) {

        mCaptureButton.setVisibility(View.VISIBLE);
        showWarning("Live preview is supported but connection failed. You may still be able to take pictures.");
    }

    @Override
    public void onLivePreviewConnectionLost(Exception e) {

        showWarning("Live preview connection lost. You may still be able to take pictures.");
    }

    @Override
    public void onPictureTakingDone() {}

    @Override
    public void onPictureTakingFailed(Exception e) {

        showMessage("Sorry, an error occurred while taking the picture.", false);
    }

    @Override
    public void onPictureDownloadProgress(float progress) {

        showMessage("Downloading picture... " + ( (int) (progress * 100) ) + " %", true);
    }

    @Override
    public void onPictureDownloadDone() {

        showMessage("Saving picture on the device...", true);
    }

    @Override
    public void onPictureDownloadFailed(Exception e) {

        showMessage("Sorry, an error occurred while downloading the picture.", false);
    }

    @Override
    public void onPictureSavingDone(InteriorImage interiorImage) {

        setResult(RESULT_PICTURE_SAVED);
        finish();
    }

    @Override
    public void onPictureSavingFailed(Exception e) {

        showMessage("Sorry, an error occurred while saving the picture on your device.", false);
    }

    @Override
    public void onTouch(View view, MotionEvent motionEvent) {}

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
