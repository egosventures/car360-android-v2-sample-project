package com.car360.sample.activities;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.car360.sample.R;
import com.egosventures.stable360.capture.fragments.Add3dTagConfirmationFragment;
import com.egosventures.stable360.core.model.entities.SpinTag;
import com.egosventures.stable360.core.views.SpinView;

import java.io.File;

public class CustomAdd3dTagConfirmationActivity extends AppCompatActivity implements Add3dTagConfirmationFragment.Add3dTagConfirmationFragmentListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    /**
     * Id of the spin to display (String, mandatory).
     */
    public static final String EXTRA_SPIN_LOCAL_ID = "EXTRA_SPIN_LOCAL_ID";

    /**
     * Index of the center frame (int, mandatory).
     */
    public static final String EXTRA_CENTER_FRAME_INDEX = "EXTRA_CENTER_FRAME_INDEX";

    /**
     * Index of the first frame (int, mandatory).
     */
    public static final String EXTRA_FIRST_FRAME_INDEX = "EXTRA_FIRST_FRAME_INDEX";

    /**
     * Index of the second frame (int, mandatory).
     */
    public static final String EXTRA_SECOND_FRAME_INDEX = "EXTRA_SECOND_FRAME_INDEX";

    /**
     * Position of the tag in the first frame (PointF, mandatory).
     */
    public static final String EXTRA_TAG_POSITION_IN_FIRST_FRAME = "EXTRA_TAG_POSITION_IN_FIRST_FRAME";

    /**
     * Position of the tag in the second frame (PointF, mandatory).
     */
    public static final String EXTRA_TAG_POSITION_IN_SECOND_FRAME = "EXTRA_TAG_POSITION_IN_SECOND_FRAME";

    /**
     * Title of the future tag (String, optional).
     */
    public static final String EXTRA_TAG_TITLE = "EXTRA_TAG_TITLE";

    /**
     * Media of the future tag (File[], optional).
     */
    public static final String EXTRA_TAG_MEDIA_FILES = "EXTRA_TAG_MEDIA_FILES";

    /**
     * This extra (SpinTag) is set in the resulting intent if the user confirms the tag creation.
     */
    public final static String EXTRA_TAG = "EXTRA_TAG";

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static String ADD_3D_TAG_CONFIRMATION_FRAGMENT_TAG = "ADD_3D_TAG_CONFIRMATION_FRAGMENT_TAG";

    private Add3dTagConfirmationFragment mAdd3dTagConfirmationFragment;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Set content view:
        setContentView(R.layout.activity_custom_add_3d_tag_confirmation);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // If the fragment does not exist, create and add it:
        if(mAdd3dTagConfirmationFragment == null) {

            // Get the intent data:
            long spinLocalId = getIntent().getLongExtra(EXTRA_SPIN_LOCAL_ID, -1);
            int centerFrameIndex = getIntent().getIntExtra(EXTRA_CENTER_FRAME_INDEX, -1);
            int firstFrameIndex = getIntent().getIntExtra(EXTRA_FIRST_FRAME_INDEX, -1);
            int secondFrameIndex = getIntent().getIntExtra(EXTRA_SECOND_FRAME_INDEX, -1);
            PointF tagPositionInFirstFrame = getIntent().getParcelableExtra(EXTRA_TAG_POSITION_IN_FIRST_FRAME);
            PointF tagPositionInSecondFrame = getIntent().getParcelableExtra(EXTRA_TAG_POSITION_IN_SECOND_FRAME);
            String tagTitle = getIntent().getStringExtra(EXTRA_TAG_TITLE);
            File[] tagMediaFiles = (File[]) getIntent().getSerializableExtra(EXTRA_TAG_MEDIA_FILES);

            mAdd3dTagConfirmationFragment = Add3dTagConfirmationFragment.newInstance(spinLocalId, centerFrameIndex, firstFrameIndex, secondFrameIndex, tagPositionInFirstFrame, tagPositionInSecondFrame, tagTitle, tagMediaFiles);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.add3dTagConfirmationFragmentContainer, mAdd3dTagConfirmationFragment, ADD_3D_TAG_CONFIRMATION_FRAGMENT_TAG)
                    .commit();
        }

        mAdd3dTagConfirmationFragment.setListener(this);

        // Listen button clicks:
        findViewById(R.id.confirmationButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onConfirmationButtonClick();
            }
        });

        findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onCancelButtonClick();
            }
        });
    }

    @Override
    public void onBackPressed() {

        // Consider that the user canceled the tag creation:
        setResult(RESULT_CANCELED);
        finish();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();
        if(id == android.R.id.home ) { this.onBackPressed(); return true; }

        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onConfirmationButtonClick() {

        // Save the tag:
        SpinTag insertedTag = mAdd3dTagConfirmationFragment.confirmTagCreation(this);

        // Add the tag to the resulting intent:
        Intent data = new Intent();
        data.putExtra(EXTRA_TAG, insertedTag);

        // Finish:
        setResult(RESULT_OK, data);
        finish();
    }

    private void onCancelButtonClick() {

        // Finish with a cancel result code:
        setResult(RESULT_CANCELED);
        finish();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    public void onInitError(Add3dTagConfirmationFragment.InitError initError) {

        Toast.makeText(this, "Sorry, an error occurred while adding your tag (error: " + initError.name() + ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSpinLoadingError(SpinView.LoadingError loadingError) {

        Toast.makeText(this, "Sorry, an error occurred while adding your tag (error: " + loadingError.name() + ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackgroundClickEndConfirmed() {

    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
