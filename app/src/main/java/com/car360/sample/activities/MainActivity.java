package com.car360.sample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.car360.sample.R;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.Stable360FrameworksHelper;
import com.egosventures.stable360.core.services.DataSyncService;
import com.egosventures.stable360.core.utils.Callback;

public class MainActivity extends AppCompatActivity {

    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static int REQUEST_CREATE_NEW_CAPTURE = 0;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate the layout:
        setContentView(R.layout.activity_main);

        findViewById(R.id.openCapturesGalleryButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onOpenCapturesGalleryButtonClick();
            }
        });

        findViewById(R.id.takeNewCaptureButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onTakeNewCaptureButtonClick();
            }
        });

        findViewById(R.id.openOnlineCaptureButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onOpenOnlineCaptureButtonClick();
            }
        });
    }

    @Override
    protected void onResume() {

        // Call super:
        super.onResume();

        // Check if the framework is init:
        Stable360FrameworksHelper.addInitListener(new Stable360Framework.InitListener() {

            @Override
            public void onInitDone() {}

            @Override
            public void onInitError(Stable360Framework.Stable360FrameworkError stable360FrameworkError) {

                Toast.makeText(MainActivity.this, "Sorry, an error occurred while initializing the Stable360 framework: " + stable360FrameworkError, Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onOpenCapturesGalleryButtonClick() {

        // If the data sync is not running, open the activity:
        DataSyncService.isRunning(this, new Callback<Boolean>() {

            @Override
            public void onResult(Boolean isRunning) {

                if(isRunning) {

                    Toast.makeText(MainActivity.this, "Data sync service is still running, please wait.", Toast.LENGTH_LONG).show();
                }
                else {

                    Intent intent = new Intent(MainActivity.this, CapturesGalleryActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void onTakeNewCaptureButtonClick() {

        // If the data sync is not running, open the activity:
        DataSyncService.isRunning(this, new Callback<Boolean>() {

            @Override
            public void onResult(Boolean isRunning) {

                if(isRunning) {

                    Toast.makeText(MainActivity.this, "Data sync service is still running, please wait.", Toast.LENGTH_LONG).show();
                }
                else {

                    Intent intent = new Intent(MainActivity.this, CreateNewCaptureActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void onOpenOnlineCaptureButtonClick() {

        Intent intent = new Intent(this, CaptureIdentificationActivity.class);
        startActivity(intent);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
