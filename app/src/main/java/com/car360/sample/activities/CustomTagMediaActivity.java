package com.car360.sample.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.car360.sample.R;
import com.commonsware.cwac.provider.StreamProvider;
import com.egosventures.stable360.core.adapters.TagMediaPagerAdapter;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.Capture;
import com.egosventures.stable360.core.model.entities.InteriorImage;
import com.egosventures.stable360.core.model.entities.Spin;
import com.egosventures.stable360.core.model.entities.Tag;
import com.egosventures.stable360.core.model.entities.TagMedia;
import com.egosventures.stable360.core.utils.Utils;

import java.io.File;
import java.util.ArrayList;

public class CustomTagMediaActivity extends AppCompatActivity {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public final static String EXTRA_SPIN = "EXTRA_SPIN";
    public final static String EXTRA_INTERIOR_IMAGE = "EXTRA_INTERIOR_IMAGE";
    public final static String EXTRA_TAG = "EXTRA_TAG";

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static int PERMISSION_REQUEST_CAMERA_TO_TAKE_PHOTO = 0;

    private final static int REQUEST_TAKE_PHOTO = 0;

    private Capture mCapture;
    private Tag mTag;
    private Spin mSpin;
    private InteriorImage mInteriorImage;

    private ArrayList<TagMedia> mTagMediaList;

    private ViewPager mViewPager;
    private TagMediaPagerAdapter mPagerAdapter;

    private File mDestination;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_tag_media);

        // Enable up navigation :
        if(getSupportActionBar() != null) { getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

        // Get the widgets:
        mViewPager = findViewById(R.id.viewPager);

        // Get the data:
        mSpin = getIntent().getParcelableExtra(EXTRA_SPIN);
        mInteriorImage = getIntent().getParcelableExtra(EXTRA_INTERIOR_IMAGE);
        mTag = getIntent().getParcelableExtra(EXTRA_TAG);

        if(mSpin == null && mInteriorImage == null) {
            throw new RuntimeException("You must either provide a spin or an interior image with the key EXTRA_SPIN or EXTRA_INTERIOR_IMAGE");
        }

        if(mSpin != null) {
            mCapture = Stable360Db.getInstance(this).getWithId(Capture.class, mSpin.getCaptureLocalId());
        }
        else {
            mCapture = Stable360Db.getInstance(this).getWithId(Capture.class, mInteriorImage.getCaptureLocalId());
        }

        // Get the media:
        mTagMediaList = mTag.getMediaSorted(this);

        // Init the adapter and the view pager :
        mPagerAdapter = new TagMediaPagerAdapter(getSupportFragmentManager(), mTagMediaList, ImageView.ScaleType.CENTER_CROP, false);
        mViewPager.setAdapter(mPagerAdapter);

        // Listen clicks:
        findViewById(R.id.addTagMediaButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTakePhotoButtonClicked();
            }
        });

        findViewById(R.id.deleteTagMediaButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteTagMediaButtonClicked();
            }
        });

        // Load the data:
        loadData(0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case PERMISSION_REQUEST_CAMERA_TO_TAKE_PHOTO: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onTakePhotoButtonClicked();
                }
                else {
                    Toast.makeText(this, "Sorry, this permission is needed to take a picture", Toast.LENGTH_LONG).show();
                }

                break;
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        // Call super:
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REQUEST_TAKE_PHOTO: {

                if (resultCode == RESULT_OK) {

                    if (mDestination.exists()) {

                        // Add the media to the tag:
                        mTag.getMedia().add(new TagMedia("", "", mTag.getMaxMediaPosition() + 1, mDestination, null, null));

                        // Update the tag:
                        Stable360Db.getInstance(this).update(mTag, mTag.getLocalId());

                        mPagerAdapter.notifyDataSetChanged();
                        loadData(mPagerAdapter.getCount() - 1);
                    }
                    else {

                        Toast.makeText(this, "Sorry, an error occurred while saving your picture", Toast.LENGTH_LONG).show();
                    }
                }

                break;
            }
        }
    }



    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { onBackPressed(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onTakePhotoButtonClicked() {

        if(Utils.checkOrRequestPermissions(this, new String[] { android.Manifest.permission.CAMERA }, PERMISSION_REQUEST_CAMERA_TO_TAKE_PHOTO)) {

            mDestination = getNewMediaFile("jpg");

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(mDestination));
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, REQUEST_TAKE_PHOTO);
        }
    }

    private void onDeleteTagMediaButtonClicked() {

        if(mPagerAdapter.getCount() > 0) {
            deleteCurrentTagMedia(mTag);
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    private void loadData(int displayedIndex) {

        if(mPagerAdapter.getCount() == 0) {
            mViewPager.setVisibility(View.GONE);
        }
        else {
            mViewPager.setVisibility(View.VISIBLE);
            mViewPager.setCurrentItem(displayedIndex);
        }
    }

    private File getNewMediaFile(String extension) {

        File destination = null;

        if(mSpin != null) {

            destination = mSpin.getNewMediaFile(this, mTag, extension);
            if(!destination.getParentFile().exists() && !destination.getParentFile().mkdirs()) {
                return null;
            }
        }
        else if(mInteriorImage != null) {

            destination = mInteriorImage.getNewMediaFile(this, mTag, extension);
            if(!destination.getParentFile().exists() && !destination.getParentFile().mkdirs()) {
                return null;
            }
        }

        return destination;
    }

    private Uri getUriFromFile(File file) {

        Uri newMediaUri;
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            newMediaUri = Uri.fromFile(file);
        }
        else {
            newMediaUri = StreamProvider.getUriForFile(getApplicationContext().getPackageName() + ".ShareFileProvider", file);
        }

        return newMediaUri;
    }

    private void deleteCurrentTagMedia(Tag tag) {

        // Get the media:
        int index = mViewPager.getCurrentItem();
        TagMedia tagMedia = tag.getMedia().get(index);

        // Remove the media:
        tag.getMedia().remove(index);

        // Update the tag:
        Stable360Db.getInstance(this).update(tag, tag.getLocalId());

        // Reload the UI:
        mPagerAdapter.notifyDataSetChanged();
        loadData(index == 0 ? 0 : index -1);

        // Delete the media file only if it is not a linked still image:
        if(!tagMedia.isLinkedStillImage(this, mCapture)) {
            if(!tagMedia.getLocalFile().exists() && !tagMedia.getLocalFile().delete()) {
                Toast.makeText(this, "Sorry, an error occurred while deleting your media.", Toast.LENGTH_LONG).show();
            }
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
