package com.car360.sample.activities;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.car360.sample.R;
import com.egosventures.stable360.capture.fragments.Add3dTagFragment;

import java.io.File;

public class CustomAdd3dTagActivity extends AppCompatActivity implements Add3dTagFragment.Add3dTagFragmentListener {

    /* ------------------------------------------------------------------------------------------ */
    // region Public attributes

    public static final String EXTRA_SPIN_LOCAL_ID = "EXTRA_SPIN_LOCAL_ID";
    public static final String EXTRA_TAG_TITLE = "EXTRA_TAG_TITLE";
    public static final String EXTRA_TAG_MEDIA_FILES = "EXTRA_TAG_MEDIA_FILES";

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Private attributes

    private final static String ADD_3D_TAG_FRAGMENT_TAG = "ADD_3D_TAG_FRAGMENT_TAG";
    private final static int REQUEST_ADD_3D_TAG_CONFIRMATION_ACTIVITY = 0;

    private Add3dTagFragment mAdd3dTagFragment;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_add_3d_tag);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // If the fragment does not exist, create and add it:
        if(mAdd3dTagFragment == null) {

            // Get the intent data:
            Long spinId = getIntent().getLongExtra(EXTRA_SPIN_LOCAL_ID, -1);
            String tagTitle = getIntent().getStringExtra(EXTRA_TAG_TITLE);
            File[] tagMediaFiles = (File[]) getIntent().getSerializableExtra(EXTRA_TAG_MEDIA_FILES);

            mAdd3dTagFragment = Add3dTagFragment.newInstance(spinId, tagTitle, tagMediaFiles);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.add3dTagFragmentContainer, mAdd3dTagFragment, ADD_3D_TAG_FRAGMENT_TAG)
                    .commit();
        }

        mAdd3dTagFragment.setListener(this);

        findViewById(R.id.okButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOkButtonClick();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Call super:
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REQUEST_ADD_3D_TAG_CONFIRMATION_ACTIVITY: {

                switch (resultCode) {

                    case RESULT_OK: {

                        // Forward the result:
                        setResult(RESULT_OK, data);
                        finish();

                        break;
                    }
                }
                break;
            }
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Add3dTagFragment.Add3dTagFragmentListener

    @Override
    public void onInitError(Add3dTagFragment.InitError initError) {

        Toast.makeText(this, "Sorry, ", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackgroundClickEndConfirmed() {}

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();
        if(id == android.R.id.home ) { this.onBackPressed(); return true; }

        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region User interaction

    private void onOkButtonClick() {

        // Get the frames indexes:
        int centerFrameIndex = mAdd3dTagFragment.getCenterFrameIndex();
        int firstFrameIndex = mAdd3dTagFragment.getFirstFrameIndex();
        int secondFrameIndex = mAdd3dTagFragment.getSecondFrameIndex();

        // Get the coordinates of the first & second frames points:
        PointF tagPositionInFirstFrame = mAdd3dTagFragment.getTagPositionInFirstFrame();
        PointF tagPositionInSecondFrame = mAdd3dTagFragment.getTagPositionInSecondFrame();

        // Create the intent:
        Intent intent = new Intent(this, CustomAdd3dTagConfirmationActivity.class);

        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_CENTER_FRAME_INDEX, centerFrameIndex);
        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_FIRST_FRAME_INDEX, firstFrameIndex);
        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_SECOND_FRAME_INDEX, secondFrameIndex);

        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_TAG_POSITION_IN_FIRST_FRAME, tagPositionInFirstFrame);
        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_TAG_POSITION_IN_SECOND_FRAME, tagPositionInSecondFrame);

        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_SPIN_LOCAL_ID, mAdd3dTagFragment.getSpin().getLocalId());
        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_TAG_TITLE, mAdd3dTagFragment.getTagTitle());
        intent.putExtra(CustomAdd3dTagConfirmationActivity.EXTRA_TAG_MEDIA_FILES, mAdd3dTagFragment.getTagMediaFiles());

        startActivityForResult(intent, REQUEST_ADD_3D_TAG_CONFIRMATION_ACTIVITY);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */
}
