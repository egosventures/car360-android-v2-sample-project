package com.car360.sample.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.car360.sample.R;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.Capture;
import com.egosventures.stable360.core.model.entities.InteriorImageStandard;
import com.egosventures.stable360.core.model.entities.Spin;
import com.egosventures.stable360.core.model.entities.SpinStandard;
import com.egosventures.stable360.core.model.entities.StillImageStandard;
import com.egosventures.stable360.core.utils.Callback;
import com.egosventures.stable360.core.utils.Utils;
import com.egosventures.stable360.core.utils.images_loader.BitmapDecoder;
import com.egosventures.stable360.core.utils.math.Size;

import java.io.File;
import java.util.ArrayList;

public class CaptureDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    /* ------------------------------------------------------------------------------------------ */
    // region View holder

    public static class CaptureViewHolder extends RecyclerView.ViewHolder {

        protected ViewGroup container;
        private ImageView imageView;
        private TextView identificationTextView;
        private ImageButton uploadCaptureButton;
        private ImageButton deleteCaptureButton;

        CaptureViewHolder(ViewGroup container, ImageView imageView, TextView identificationTextView, ImageButton uploadCaptureButton, ImageButton deleteCaptureButton) {

            super(container);

            this.container = container;
            this.imageView = imageView;
            this.identificationTextView = identificationTextView;
            this.uploadCaptureButton = uploadCaptureButton;
            this.deleteCaptureButton = deleteCaptureButton;
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Public interfaces

    public interface CaptureDetailsAdapterListener {

        void onSpinStandardClick(long standardId);
        void onInteriorImageStandardClick(long standardId);
        void onStillImageStandardClick(long standardId);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Attributes

    private final static int VIEW_TYPE_SPIN_STANDARD = 0;
    private final static int VIEW_TYPE_INTERIOR_IMAGE_STANDARD = 1;
    private final static int VIEW_TYPE_STILL_IMAGE_STANDARD = 2;

    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private CaptureDetailsAdapterListener mListener;
    private Capture mCapture;
    private ArrayList<SpinStandard> mSpinStandards;
    private ArrayList<InteriorImageStandard> mInteriorImageStandards;
    private ArrayList<StillImageStandard> mStillImageStandards;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Constructors

    public CaptureDetailsAdapter(Activity activity, RecyclerView recyclerView, long captureLocalId, CaptureDetailsAdapterListener listener) {

        mActivity = activity;
        mRecyclerView = recyclerView;
        mListener = listener;

        mCapture = Stable360Db.getInstance(activity).getWithId(Capture.class, captureLocalId);

        reloadData();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region View holder

    @NonNull
    @Override
    public CaptureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // Get the layout inflater:
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {

            case VIEW_TYPE_SPIN_STANDARD: {

                break;
            }
        }

        // Create a new view:
        CardView container = (CardView) inflater.inflate(R.layout.activity_capture_details_spin_standard_item, parent, false);

        // Get the widgets:
        ImageView imageView = (ImageView) container.findViewById(R.id.imageView);
        TextView identificationTextView = (TextView) container.findViewById(R.id.captureIdentificationTextView);
        ImageButton uploadCaptureButton = (ImageButton) container.findViewById(R.id.uploadCaptureButton);
        ImageButton deleteCaptureButton = (ImageButton) container.findViewById(R.id.deleteCaptureButton);

        container.setOnClickListener(this);
        uploadCaptureButton.setOnClickListener(this);
        deleteCaptureButton.setOnClickListener(this);
        container.setOnClickListener(this);

        return new CaptureViewHolder(container, imageView, identificationTextView, uploadCaptureButton, deleteCaptureButton);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {

        //return mSpinStandards.size() + mInteriorImageStandards.size() + mStillImageStandards.size() + 3;
        return mSpinStandards.size();
    }

    @Override
    public int getItemViewType(int position) {

        if(position < mSpinStandards.size()) return VIEW_TYPE_SPIN_STANDARD;
        if(position < mSpinStandards.size() + mInteriorImageStandards.size()) return VIEW_TYPE_INTERIOR_IMAGE_STANDARD;
        if(position < mSpinStandards.size() + mInteriorImageStandards.size() + mStillImageStandards.size()) return VIEW_TYPE_STILL_IMAGE_STANDARD;

        return -1;
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region View.OnClickListener

    @Override
    public void onClick(View view) {

    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    private void reloadData() {

        Stable360Db db = Stable360Db.getInstance(mActivity);
        mSpinStandards = db.getSpinStandardsWithWorkflowMetadataId(mCapture.getWorkflowMetadataId());
        mInteriorImageStandards = db.getInteriorImageStandardsWithWorkflowMetadataId(mCapture.getWorkflowMetadataId());
        mStillImageStandards = db.getStillImageStandardsWithWorkflowMetadataId(mCapture.getWorkflowMetadataId());

        notifyDataSetChanged();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */

}
