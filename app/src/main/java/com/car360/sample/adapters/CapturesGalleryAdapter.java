package com.car360.sample.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.car360.sample.R;
import com.egosventures.stable360.core.model.Stable360Db;
import com.egosventures.stable360.core.model.entities.Capture;
import com.egosventures.stable360.core.utils.Callback;
import com.egosventures.stable360.core.utils.Utils;
import com.egosventures.stable360.core.utils.images_loader.BitmapDecoder;
import com.egosventures.stable360.core.utils.math.Size;

import java.io.File;
import java.util.ArrayList;

public class CapturesGalleryAdapter extends RecyclerView.Adapter<CapturesGalleryAdapter.CaptureViewHolder> implements View.OnClickListener {

    /* ------------------------------------------------------------------------------------------ */
    // region View holder

    public static class CaptureViewHolder extends RecyclerView.ViewHolder {

        protected ViewGroup container;
        private ImageView imageView;
        private TextView identificationTextView;
        private ImageButton uploadCaptureButton;
        private ImageButton deleteCaptureButton;

        CaptureViewHolder(ViewGroup container, ImageView imageView, TextView identificationTextView, ImageButton uploadCaptureButton, ImageButton deleteCaptureButton) {

            super(container);

            this.container = container;
            this.imageView = imageView;
            this.identificationTextView = identificationTextView;
            this.uploadCaptureButton = uploadCaptureButton;
            this.deleteCaptureButton = deleteCaptureButton;
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Public interfaces

    public interface CapturesGalleryAdapterListener {

        void onCaptureClick(long captureId);
        void onDeleteCaptureButtonClick(long captureId);
        void onUploadCaptureButtonClick(long captureId);
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Attributes

    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private CapturesGalleryAdapterListener mListener;
    private ArrayList<Capture> mCaptures;

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Constructors

    public CapturesGalleryAdapter(Activity activity, RecyclerView recyclerView, CapturesGalleryAdapterListener listener) {

        mActivity = activity;
        mRecyclerView = recyclerView;
        mListener = listener;

        reloadData();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region View holder

    @NonNull
    @Override
    public CaptureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // Get the layout inflater:
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // Create a new view:
        CardView container = (CardView) inflater.inflate(R.layout.activity_captures_gallery_capture_item, parent, false);

        // Get the widgets:
        ImageView imageView = container.findViewById(R.id.imageView);
        TextView identificationTextView = container.findViewById(R.id.captureIdentificationTextView);
        ImageButton uploadCaptureButton = container.findViewById(R.id.uploadCaptureButton);
        ImageButton deleteCaptureButton = container.findViewById(R.id.deleteCaptureButton);

        container.setOnClickListener(this);
        uploadCaptureButton.setOnClickListener(this);
        deleteCaptureButton.setOnClickListener(this);
        container.setOnClickListener(this);

        return new CaptureViewHolder(container, imageView, identificationTextView, uploadCaptureButton, deleteCaptureButton);
    }

    @Override
    public void onBindViewHolder(@NonNull CaptureViewHolder holder, int position) {

        Capture capture = mCaptures.get(position);

        final CaptureViewHolder captureViewHolder = (CaptureViewHolder) holder;

        File oldFile = (File) captureViewHolder.imageView.getTag();
        final File newFile = capture.getPreviewImage(mActivity);
        final boolean fileChanged = newFile == null || oldFile == null || oldFile.lastModified() != newFile.lastModified();

        captureViewHolder.container.setTag(capture.getLocalId());
        captureViewHolder.identificationTextView.setText(capture.getCarIdentification().getValue());
        captureViewHolder.deleteCaptureButton.setTag(capture.getLocalId());
        captureViewHolder.uploadCaptureButton.setTag(capture.getLocalId());
        captureViewHolder.imageView.setTag(newFile);

        if(fileChanged) {
            captureViewHolder.imageView.setImageBitmap(null);
            captureViewHolder.imageView.setAlpha(0f);
        }

        Utils.getViewSize(captureViewHolder.imageView, new Callback<Size>() {

            @Override
            public void onResult(final Size size) {

                if(newFile != null && newFile.exists()) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final Bitmap bitmap = BitmapDecoder.decodeSampledBitmapFromFile(newFile.getAbsolutePath(), size, true);

                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    captureViewHolder.imageView.setImageBitmap(bitmap);

                                    if(fileChanged) {
                                        captureViewHolder.imageView.animate().alpha(1f).setDuration(300);
                                    }
                                }
                            });
                        }
                    }).start();
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return mCaptures.size();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region View.OnClickListener

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.deleteCaptureButton) {

            Long captureId = (Long) view.getTag();
            mListener.onDeleteCaptureButtonClick(captureId);
        }
        else if(view.getId() == R.id.uploadCaptureButton) {

            Long captureId = (Long) view.getTag();
            mListener.onUploadCaptureButtonClick(captureId);
        }
        else if(view.getId() == R.id.containerCardView) {

            Long captureId = (Long) view.getTag();
            mListener.onCaptureClick(captureId);
        }
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */



    /* ------------------------------------------------------------------------------------------ */
    // region Utils

    public void reloadData() {

        // Get the captures:
        mCaptures = Stable360Db.getInstance(mActivity).getCapturesOfCurrentUser();
        notifyDataSetChanged();
    }

    // endregion
    /* ------------------------------------------------------------------------------------------ */

}
