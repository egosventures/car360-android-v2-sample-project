package com.car360.sample.application;

import android.app.Application;
import android.util.Log;

import com.egosventures.stable360.capture.Stable360CaptureFramework;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.Stable360FrameworksHelper;
import com.egosventures.stable360.core.services.DataSyncService;

public class SampleApplication extends Application {

    private final static String TAG = "SampleApplication";

    private static final String USER_EMAIL = "YOUR_EMAIL";
    private static final String USER_PASSWORD = "YOUR_PASSWORD";

    @Override
    public void onCreate() {

        Log.i(TAG, "onCreate");

        // Call super:
        super.onCreate();

        // Init the capture framework:
        Stable360CaptureFramework.getInstance(this).init(USER_EMAIL, USER_PASSWORD);

        Stable360FrameworksHelper.addInitListener(new Stable360Framework.InitListener() {

            @Override
            public void onInitDone() {

                Log.i(TAG, "framework onInitDone");

                // Start the DataSyncService:
                DataSyncService.start(SampleApplication.this, true);
            }

            @Override
            public void onInitError(Stable360Framework.Stable360FrameworkError stable360FrameworkError) {

                Log.e(TAG, "framework onInitError " + stable360FrameworkError);
            }
        });
    }
}
