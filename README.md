# CAR360 Android V2 Sample App #

A sample app showing how to use the Android CAR360 framework

### How do I get set up? ###

* Enter your Maven username and password in the project's build.gradle (located in the root folder)
* Enter your Car360 email and password in SampleApplication.java (USER_EMAIL & USER_PASSWORD)
* Run the app (on a device only)

### Who do I talk to? ###

* [bruno@egosventures.com](mailto:bruno@egosventures.com)
* [remy@egosventures.com](mailto:remy@egosventures.com)

### Links ###
* [Car360.net](http://car360.net)
* [Car360 Android framework documentation](http://doc.car360app.com/AndroidV2)
